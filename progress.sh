#!/bin/bash
#set -e

qcd=4
for i in {0..100}
do echo -n $i" " 
#grep "TChain" kinscan_QCD2_${1}/stdout/kinescan*_${i}.stdout
#grep -Po 'TChain entries : \K.*' kinscan_QCD2_${1}/stdout/kinescan*_${i}.stdout | tr -d '\n'
grep -Po 'TChain entries : \K.*' kinscan_QCD${qcd}_${1}/stdout/kinescan*_${i}.stdout | tr -d '\n'

echo -n "   "
out=`tail -n20000 kinscan_QCD${qcd}_${1}/stdout/kinescan*_${i}.stdout | grep "entry\|finished"`
if [ -z "$out" ]
then 
echo ""
else 
tail -n20000 kinscan_QCD${qcd}_${1}/stdout/kinescan*_${i}.stdout | grep "entry\|finished" |tail -n1
fi
if [ $? -eq 1 ]
then
echo ""
fi
done 

# Cut Optimization

for cut optimizer

- first compile using the "make" command.

- then run scan.py using
``` ./scan.py -s ModelA```
options are ```[ModelA, ModelB, ModelD1-5, QCD]```

Then run ```./massjobs.sh```
    - QCD estimate: 4 hours, 8 jobs per cut
    - Signal: 10mins, 1 job per cut

- you can change the scanning parameters in scan.py
the default is just to run one cut set.  look at the commented lines to run multiple cut sets
    - cutmin: lowest value of the cut
    - step: amount of increment
    - nstep: number of cut values
    - dirsuffix: results store in folder with the suffix

The final number of cuts is the product of nstep array (so don't put 0 in it)



- After all jobs finish, to merge the resultant histograms
```
./loop.sh
```
or
```
condor_submit condorloop.jdl
```

- To calculate SrootB or other metric, run : 
 (best on local computer with all files downloaded)  
```
  root -l SrootB3.C
```
- configuration in SrootB3.C
    - int const numcutset :  number of jobs/cutsets
    - int isrb: significance statistics selection
        -  0: S/sqrt(B)
        -  1: S/sqrt(S+B)
        -  2: metric sqrt2K

- SrootB4.C shows the cut flow of a certain model with certain cut
  The last line from SrootB3 can be copied straight into the following parameters:
    - int bestcut: cut set number with the most significance
    - int bestmod: model number with

#include"paramSet.h"
// global variables

// for root

//TTree          *fChain;   //!pointer to the analyzed TTree or TChain         
//Int_t           fCurrent; //!current Tree number in a TChain  


// for codes like EMJselect
//float goalintlum=20.; // target integrated luminosity
int nbin;
float* xsec;
int* nfiles;
int* nlines;
std::string* binnames;
std::string* cfname;
std::string ohistname;
bool hasPre;




int main(int argc, char *argv[])
{ 
  int   procss   = (int)atoi((argv[1])); // job number: HT bins
  char* holder   = (argv[2]);
  size_t   incutset  = (size_t)atoi((argv[3]));
  char* parmfile = (argv[4]); 
  char* psfile   = (argv[5]); 

  //Parmset ps;
  std::ifstream infile1(psfile);
  std::vector<Parmset> ParmsetV((std::istream_iterator<Parmset>(infile1)),
                                    std::istream_iterator<Parmset>());

  //std::cout<<" cut set number is "<<icutset<<std::endl;
  assert(incutset==ParmsetV.size());
  std::cout<<"job number: "<<procss<<" folder: "<<holder<<std::endl;
  std::cout<<" total number cut sets is "<<incutset<<std::endl;
  std::cout<<"input file is "<<parmfile<<std::endl;
  std::cout<<"output dir is "<<holder<<std::endl;
  std::string outdir(holder); std::ifstream infile; std::string line;

  infile.open(parmfile);
  assert(infile.is_open()); //assert!: config file must be open
    
  // Kak note: read line from param txt file one by one

  /*// line1: fill hasPre boolean
  getline(infile,line);
  std::istringstream(decomment(line))>>hasPre;
  if(!hasPre) std::cout<<" unable to normalize since missing hasPre"<<std::endl;
*/
  // line1: number of HT bins
  getline(infile,line);
  std::istringstream(decomment(line))>>nbin;
  std::cout<<"total number of HT bins: "<<nbin<<std::endl;


  std::istringstream aline(decomment(line));
  /*// line3: xsection
  xsec = new float[nbin];
  getline(infile,line);
  std::cout<<line<<std::endl;
  for(int i=0;i<nbin;i++) aline>>xsec[i];
  for(int i=0;i<nbin;i++) std::cout<<"  xsec["<<i<<"]="<<xsec[i]<<std::endl;
*/

  // line2: no of files for each bin
  nfiles = new int[nbin];
  getline(infile,line);
  aline.str(decomment(line).c_str());
  std::cout<<line<<std::endl;
  for(int i=0;i<nbin;i++) aline>>nfiles[i];
  for(int i=0;i<nbin;i++) std::cout<<"  nfiles["<<i<<"]="<<nfiles[i]<<std::endl;

  // line3: no of lines to read 
  nlines = new int[nbin];
  getline(infile,line);
  aline.str(decomment(line).c_str());
  std::cout<<line<<std::endl;
  for(int i=0;i<nbin;i++) aline>>nlines[i];
  for(int i=0;i<nbin;i++) std::cout<<"  nlines["<<i<<"]="<<nlines[i]<<std::endl;

  // line3: binnames
  binnames = new std::string[nbin];
  getline(infile,line);
  aline.str(decomment(line).c_str());
  std::cout<<line<<std::endl;
  for(int i=0;i<nbin;i++) aline>>binnames[i];
  for(int i=0;i<nbin;i++) std::cout<<"  binnames["<<i<<"]="<<binnames[i]<<std::endl;

  // line 4: text list of nutple paths; fill TChain
  cfname = new std::string[nbin];
  getline(infile,line);
  aline.str(decomment(line).c_str());
  std::cout<<line<<std::endl;
  for(int i=0;i<nbin;i++) aline>>cfname[i];
  for(int i=0;i<nbin;i++) std::cout<<" cfname["<<i<<"]="<<cfname[i]<<std::endl;

 int ipcss = 0;
 int ibin = 0;
 int iline= 0;
 for(;ibin<nbin;ibin++){
    if (nfiles[ibin]>(procss-ipcss)*nlines[ibin]){ iline = nlines[ibin]*(procss-ipcss);break;}
    int tmp=nfiles[ibin]/nlines[ibin];
    if (nfiles[ibin]%nlines[ibin]>0) tmp++;
    ipcss+=tmp;
    std::cout<< "ibin"<<ibin <<" nfiles "<<nfiles[ibin]<<" "<<nlines[ibin]<<" binjob number:"<<tmp<<
	" current process tally"<<ipcss<<std::endl; 
 }
 std::cout<< "ibin "<<ibin<<" iline "<<iline<<" process "<<procss<<" ipcss "<<ipcss<<std::endl;
 if (procss>ipcss&&iline==0) {return 0;}
 if (ibin==nbin) { std::cout<< "ibin=nbin quitting"<<std::endl; return 0;}
 // Fill TChain
  TChain* ch = new TChain("emJetAnalyzer/emJetTree");
  std::cout<<"input config file is: "<<cfname[ibin]<<std::endl;
  std::ifstream inputconfig(cfname[ibin]);
  int linecounter = 0;
  int filecounter = 0;
  std::string inputfile,outputfile;
  TH1F* hTrig = new TH1F("h1","h1",10,0,10);
  TFile* f1in;
  while(std::getline(inputconfig, inputfile))
  { 
    if(linecounter>=iline&&linecounter<iline+nlines[ibin]){
      std::cout<<"input file is "<<inputfile<<std::endl;
      f1in = new TFile(inputfile.c_str());
      filecounter+=1;
      if(linecounter==iline) {
	 //hTrig = static_cast<TH1F*>(f1in->Get("eventCountPreTrigger/eventCountPreTrigger"))->Clone();
	 hTrig =(TH1F*)(f1in->Get("eventCountPreTrigger/eventCountPreTrigger")->Clone());
 	 hTrig->SetDirectory(0);
      } else {
        TH1F* tmp = static_cast<TH1F*>( f1in->Get("eventCountPreTrigger/eventCountPreTrigger")->Clone());
        hTrig->Add(tmp);
      }
      f1in->Close();
      ch->Add(inputfile.c_str());
      std::cout<< "entries til now: "<<ch->GetEntries()<<std::endl; 
    }
    linecounter += 1;
  }
  std::cout<<nfiles[ibin]<<"linecounter: "<<linecounter<<std::endl;
  assert(nfiles[ibin]==linecounter); // ASSERT!: linecounter must agree with Nfiles
  assert(nlines[ibin]>=filecounter); // ASSERT!: added file must be less than nlines
  outputfile=outdir+"histos"+binnames[ibin]+"-"+std::to_string(procss)+".root";
  std::cout<<"output file is "<<outputfile<<std::endl;

  // SUMhist name: not used anymore
 /* {
  getline(infile,line);
  aline.str(decomment(line).c_str());
  //std::cout<<line<<std::endl;
  aline>>ohistname;
  //std::cout<<"ohistname is "<<sumhist<<std::endl;
  //ohistname=sumhist+"-"+std::to_string(icutset)+"_"+std::to_string(procss)+".root";
  //std::cout<<" output histogram is "<<ohistname<<std::endl;
  }*/

  int icount= EMJcounter(ch, ParmsetV,outputfile.c_str(), hTrig);

  std::cout<<"Job finishes with count: "<<icount<<std::endl;

}
